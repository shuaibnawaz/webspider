<?php

class Application_Form_Analyze extends Zend_Form
{

    public function init(){
    	$this->setMethod('post');

        $this->setAttrib('class', 'well form-horizontal');

    	$this->addElement('file', 'keywords', array(
    		'label' => 'Select a text file containing keywords',
    		'required' => TRUE,
            'attribs' => array('class' => 'input-file'),
            'validators' => array(
                array(
                    'validator' => 'Extension',
                    'options' => array(
                        'extension' => 'txt',
                        'messages' => array('fileExtensionFalse' => 'Invalid file extention, only txt file is supported')
                    )
                )
            )
    	));

    	$this->addElement('textarea', 'urls', array(
    		'label' => 'Edit URL(s) one item per line',
            'attribs' => array('rows' => '4', 'cols' => '180', 'class' => 'input-xlarge'),
    		'required' => TRUE,
            'validators' => array(
                new App_Validate_ValidMultilineUrls()
            )
    	));

        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Analyze',
            'attribs' => array('class' => 'btn btn-primary'),
        ));
    }
}

