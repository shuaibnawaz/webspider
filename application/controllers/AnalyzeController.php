<?php

class AnalyzeController extends Zend_Controller_Action
{

    public function init(){
        /* Initialize action controller here */
    }

    public function indexAction(){
        $form = new Application_Form_Analyze();
        if ($this->getRequest()->isPost()){
            $post = $this->getRequest()->getPost();
            if($form->isValid($post)){
                $values = $form->getValues();
                if( $form->keywords->isUploaded() ){
                    $path = $form->keywords->getFileName();
                    $values = $form->getValues();
                    $file_content = file_get_contents($path);
                    if($file_content){
                        $keywords = preg_split('/[\s]+/', $file_content);
                        $urls = preg_split('/[\s]+/', $values['urls']);
                        $crawler = new App_PhpCrawler_SurferCrawler();
                        $analysed = array();
                        foreach($urls as $url){
                            $crawler->setURL($url);
                            $crawler->addContentTypeReceiveRule("#text/html#");
                            $crawler->addURLFilterRule("#(jpg|jpeg|gif|png)$# i");
                            $crawler->addURLFilterRule("#(js|css)$# i");
                            $crawler->enableCookieHandling(true);
                            $crawler->setTrafficLimit(1000 * 1024);
                            $crawler->go();
                            $doc_info = $crawler->getDocumentInfo();

                            $keyword_frequency = array();
                            if ( !empty($doc_info->content) ){
                                $document_text = strtolower(strip_tags($doc_info->content));
                                Zend_Debug::dump($document_text);
                                foreach($keywords as $word){
                                    $whole_words = array();
                                    preg_match_all("/\b$word\b/",$document_text, $whole_words);
                                    $keyword_frequency[$word] = count($whole_words[0]);
                                }
                                $attribs = array('img[alt]', 'img[title]', 'a[alt]', 'a[title]');
                                $this->analyzeAttribs($attribs, $keywords, $doc_info->content, $keyword_frequency);

                                arsort($keyword_frequency);
                                $analysed[] = array(
                                    'url' => $url,
                                    'keywords' => $keyword_frequency,
                                );
                            }else{
                                $analysed[] = array(
                                    'url' => $url,
                                    'error' => 'There is some issue with crawling the url. <em>%url</em>',
                                );
                            }
                        }
                        $this->view->analysed = $analysed;
                    }
                }
            }
        }

        $this->view->form = $form;
    }

    public function alternateAction () {
        $form = new Application_Form_Analyze( );
        if ($this->getRequest()->isPost()){
            $post = $this->getRequest()->getPost();
            if($form->isValid($post)){
                $values = $form->getValues();
                if( $form->keywords->isUploaded() ){
                    $path = $form->keywords->getFileName();
                    $values = $form->getValues();
                    $file_content = file_get_contents($path);
                    if($file_content){
                        $keywords = preg_split('/[\s]+/', $file_content);
                        $urls = preg_split('/[\s]+/', $values['urls']);
                        Zend_Loader::loadFile('SimpleHtmlDom/simple_html_dom.php');
                        $analysed = array();
                        foreach($urls as $url){
                            $html = file_get_html($url)->plaintext;
                            $analysed_keywords = array();
                            if ( !empty($html) ){
                                foreach($keywords as $word){
                                    $whole_words = array();
                                    preg_match_all("/\b$word\b/",$html, $whole_words);
                                    $keyword_frequency[$word] = count($whole_words[0]);
                                }
                                arsort($analysed_keywords);
                                $analysed[] = array(
                                    'url' => $url,
                                    'keywords' => $analysed_keywords,
                                );
                            }else{
                                $analysed[] = array(
                                    'url' => $url,
                                    'error' => 'There is some issue crawling the url. <em>%url</em>',
                                );
                            }
                        }
                        $this->view->analysed = $analysed;
                    }
                }
            }
        }

        $this->view->form = $form;
    }

    private function analyzeAttribs($attribs, $keywords, $html, &$results){
        Zend_Loader::loadFile('SimpleHtmlDom/simple_html_dom.php');
        $s = strpos($html, '<body');
        $e = strpos($html, '</body>');
        $body_tag = substr($html, $s, $e-$s+7);
        $dom = str_get_html($body_tag);
        $attrib_text = "";

        foreach( $attribs as $attr ){
            foreach( $dom->find($attr) as $elem ){
                $asPos = strpos($attr, '[')+1;
                $aePos = strpos($attr, ']');
                $attrib_name = substr($attr, $asPos, $aePos-$asPos);
                $attrib_text .= $elem->getAttribute($attrib_name) . "\r\n";
            }
        }

        foreach( $keywords as $word ){
            $whole_words = array();

            preg_match_all("/\b$word\b/",strtolower($attrib_text), $whole_words);

            if( is_int($results[$word]) )
                $results[$word] = $results[$word] + count($whole_words[0]);
            else
                $results[$word] = count($whole_words[0]);
        }
    }
}



