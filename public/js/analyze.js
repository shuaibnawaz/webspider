SurferNet = {};
SurferNet.AnalysisPanel = {};
SurferNet.SitemapPanel = {};

SurferNet.DictionaryManager = function DictionaryManager(){
    var collection = {};
    var $ = jQuery;

    this.addItem = function(_context, _key, _object){
        var md5Key = MD5(_key);
        _object.url = _key;
        if ( !collection.hasOwnProperty(_context) ){
            collection[_context] = {};
        }
        collection[_context][md5Key] = _object;
    };

    this.initDictionary = function(_context){
        collection[_context] = {};
    };

    this.removeItem = function(_context, _key){
        var md5Key = MD5(_key);
        if ( !collection.hasOwnProperty(_context) ){
            throw "The collection is not initialized yet.";
            return false;
        }

        if ( !collection[_context].hasOwnProperty(md5Key) ){
            throw "The item identified doesn't exists in the collection.";
            return false;
        }

        delete collection[_context][md5Key];
    };

    this.editItem = function(_context, _key, _object){
        var md5Key = MD5(_key);

        if ( !collection.hasOwnProperty(_context) ){
            throw "The collection is not initialized yet.";
            return false;
        }

        if ( !collection[_context].hasOwnProperty(md5Key) ){
            throw "The item identified doesn't exists in the collection.";
            return false;
        }

        if ( _object.hasOwnProperty('oldUrl') ){
            var oldMd5Key = MD5(_object.oldUrl);
            var oldUrl = _object.oldUrl;
            delete _object.oldUrl;
            delete collection[_context][oldMd5Key];
            this.addItem(_object.url, _object);
        }

        $.extend(true, collection[_context][md5Key], _object);
    };

    this.getCount = function(_context){
        if ( !collection.hasOwnProperty(_context) ){
            throw "The collection is not initialized yet.";
            return false;
        }

        var counts = 0;

        for(p in collection[_context]){
            if( collection[_context].hasOwnProperty(p) ){
                counts++;
            }
        }
        return counts;
    };

    this.getItem = function(_context, _key){
        var md5Key = MD5(_key);
        if ( !collection.hasOwnProperty(_context) ){
            throw "The collection is not initialized yet.";
            return false;
        }

        if ( !collection[_context].hasOwnProperty(md5Key) ){
            throw "The item identified doesn't exists in the collection.";
            return false;
        }

        return collection[_context][md5Key];
    };

    this.itemExists = function(_context, _key){
        var md5Key = MD5(_key);
        if ( !collection.hasOwnProperty(_context) ){
            throw "The collection is not initialized yet.";
            return false;
        }

        if ( !collection[_context].hasOwnProperty(md5Key) ){
            return false;
        }else{
            return true;
        }
    };
}

SurferNet.DictionaryInterface = new SurferNet.DictionaryManager();

SurferNet.Dictionary = function Dictionary(_name){
    if ( typeof _name == 'undefined' ){
        throw "Invalid initialization without any identification. " +
        "Dictionary expects one parameter as context identifier.";
        return false;
    }

    SurferNet.DictionaryInterface.initDictionary(_name);

    this.addItem = function(_key, _object){
        return SurferNet.DictionaryInterface.addItem(_name, _key, _object);
    };

    this.removeItem = function(_key){
        return SurferNet.DictionaryInterface.removeItem(_name, _key);
    };

    this.editItem = function(_key, _object){
        return SurferNet.DictionaryInterface.editItem(_name, _key, _object);
    };

    this.getCount = function(){
        return SurferNet.DictionaryInterface.getCount(_name);
    };

    this.getItem = function(_key){
        return SurferNet.DictionaryInterface.getItem(_name, _key);
    };

    this.itemExists = function(_key){
        return SurferNet.DictionaryInterface.itemExists(_name, _key);
    };
};

SurferNet.AnalysisPanel.StatsManager = function StatsManager(){
    $ = jQuery;

    this.setStats = function(_element, _stats, _accumulate){
        $.data(_element, 'stats', _stats);
        //if (_accumulate )
    };

    this.getStats = function(_element){
        return $.data(_element, 'stats');
    };

    this.render = function(_element, _stats){};

    this.getCumulativeStats = function(_element){
        if( $(_element).is('ul.group') ){
            return $.data(_element, 'stats');
        }
    };

    this.accumulate = function(_element, _stats){
        // Process to recursively merge one item stats (_stats) to cumulative data.
    };
};

SurferNet.AnalysisPanel.GroupList = function GroupList(_selector, _options){
    var $ = jQuery;
    var $element = $(_selector);
    var element = $element.get(0);
    var instance = $.data(element, 'instance');
    if (instance instanceof GroupList) {
        return instance;
    }
    var self = this;
    var options = _options;
    var $groups = $('> ul', element);

    $groups.each(function(){
        new AnalysisPanel.Group(this, {groupList: instance});
    });

    this.getOption = function(key){
        return options[key];
    };

    this.setKeywords = function(_keywords){
        $.data(element, 'keywords', _keywords);
    };

    this.getKeywords = function(){
        return $.data(element, 'keywords');
    };

    $.data(element, 'instance', this);
};

SurferNet.AnalysisPanel.Group = function Group(_selector, _options){
    var $ = jQuery;
    var $element = $(_selector);
    var element = $element.get(0);
    var instance = $.data(element, 'instance');
    if (instance instanceof Group){
        return instance;
    }
    var self = this;
    var options = _options;
    var $items = $('> li', element);

    $items.each(function(){
        new AnalysisPanel.Item(this, {groupList: options.groupList, group: instance});
    });

    this.getOption = function(key){
        return options[key];
    };

    $.data(element, 'instance', this);
};

SurferNet.AnalysisPanel.Item = function Item(_selector, _options){
    var $ = jQuery;
    var $element = $(_selector);
    var element = $element.get(0);
    var instance = $.data(element, 'instance');
    if (instance instanceof Item){
        return instance;
    }
    var self = this;
    var options = _options;
    var $url = $('> span', element);
    var $stats = $('> div', element);
    this.url = $url.text();

    this.crawl = function(_next){
        var crawlerUrl = options.groupList.getOption('crawler');
        //$.post(crawlerUrl, data, function(){
        //
        //});
    };

    $element.click(function(){
        //options.groupList.displayStats(element);
    });

    $.data(element, 'instance', this);
};

SurferNet.SitemapPanel.GridRow = function GridRow(_values){
    if ( !_values.hasOwnProperty('url') ){
        throw "A required property 'url' is missing in values.";
        return false;
    }

    this.serial = _values.hasOwnProperty('serial') ? _values.serial : null;
    this.url = _values.url;
    this.priority = _values.hasOwnProperty('priority') ? _values.priority : 1;
    this.frequency = _values.hasOwnProperty('frequency') ? _values.frequency : 'daily';
    this.state = _values.hasOwnProperty('state') ? _values['state'] : 0;
    this.depth = _values.hasOwnProperty('depth') ? _values.depth : 0;

    //this.rawValues = _values;

    this.merge = function(_updates){
        if ( _updates.hasOwnProperty('url') && _updates.url != this.url){
            this.oldUrl = this.url;
        }

        for(p in _updates){
            this[p] = _updates[p];
        }
    };

};

SurferNet.SitemapPanel.Grid = function Grid(_selector, _autoInitialize){
    var dict = new SurferNet.Dictionary('sitemap');
    var $ = jQuery;
    var $table = $(_selector);
    var $tbody = $table.find('tbody');
    var uiMan = new SurferNet.SitemapPanel.UIManager($tbody.get(0));

    var autoInitialize = function(){
        $tbody.find('tr').each(function(_index){
            var row = uiMan.loadRow(_index);
            dict.addItem(row.url, row);
        });
    };

    this.addRow = function(_values){
        var row = new SurferNet.SitemapPanel.GridRow(_values);
        dict.addItem(row.url, row);
        uiMan.createRow(row);
    };

    this.removeRow = function(_index) {
        var row = uiMan.loadRow(_index);
        dict.removeItem(row.url);
        uiMan.destroyRow(_index);
    };

    this.editRow = function(_index, _values, _partial) {
        var row = null;
        if ( typeof _partial != 'undefined' ){
            row = uiMan.loadRow(_index);
            row.merge(_values);
        }else{
            row = new SurferNet.SitemapPanel.GridRow(_values);
        }

        dict.editItem(row.url, row);
        uiMan.updateRow(_index, row);
    };

    this.countRows = function() {
        return dict.getCount();
    };

    this.getRow = function(_index) {
        return uiMan.loadRow(_index);
    };

    this.nextIndex = function() {
        return parseInt($tbody.find('tr:last-child').attr('id').split('-')[1]) + 1;
    };

    if (typeof _autoInitialize != 'undefined' && _autoInitialize){
        autoInitialize();
    }
};

SurferNet.SitemapPanel.UIManager = function UIManager(_tableBody){
    $ = jQuery;
    $tbody = $(_tableBody);

    this.loadRow = function(_index){
        $rows = $tbody.find('tr');
        var values = {
            serial : parseInt($($rows[_index]).find('td.serial').text()),
            url: $($rows[_index]).find('td.url').text(),
            priority: parseInt($($rows[_index]).find('td.priority input').val()),
            frequency: $($rows[_index]).find('td.frequency select').val().toLowerCase(),
            state: $($rows[_index]).find('td.state span').text().toLowerCase(),
            depth: parseInt($($rows[_index]).find('td.depth').text())
        };

        var row = new SurferNet.SitemapPanel.GridRow(values);
        return row;
    };

    this.createRow = function(_row){
        var trHtml = getTemplate('row');
        trHtml = trHtml.replace(/%serial/g, _row.serial);
        trHtml = trHtml.replace(/%url/g, _row.url);
        trHtml = trHtml.replace(/%depth/g, _row.depth);
        trHtml = trHtml.replace(/%priority/g, getTemplate('priority'));
        trHtml = trHtml.replace(/%priority_value/g, _row.priority);
        trHtml = trHtml.replace(/%frequency/g, getTemplate('frequency'));

        switch(_row.state){
            case "success":
                trHtml = trHtml.replace(/%state/g, getTemplate('stateSuccess'));
                break;
            case "processing":
                trHtml = trHtml.replace(/%state/g, getTemplate('stateProcessing'));
                break;
            case "failure":
                trHtml = trHtml.replace(/%state/g, getTemplate('stateFailure'));
                break;
            case "pending":
            default:
                trHtml = trHtml.replace(/%state/g, getTemplate('statePending'));
                break;
        }

        $tbody.append(trHtml);
        if ( _row.hasOwnProperty('frequency') ){
            $tbody.find('tr#row-'+ _row.serial + ' td.frequency select').val(_row.frequency);
        }
    };

    this.destroyRow = function(_index){
        $rows = $tbody.find('tr');
        $($rows[_index]).remove();
    };
    this.updateRow = function(_index, _row){ };

    var getTemplate = function(_type){
        return templates[_type];
    }

    var templates = {
        row: '<tr id="row-%serial">' +
                '<td class="serial">%serial</td>' +
                '<td class="url">%url</td>' +
                '<td class="priority">%priority</td>' +
                '<td class="frequency">%frequency</td>' +
                '<td class="state">%state</td>' +
                '<td class="depth">%depth</td>' +
             '</tr>',
        priority: '<input class="span12" type="number" value="%priority_value">',
        frequency: '<select class="span12">' +
                            '<option value="always">Always</option>' +
                            '<option value="hourly">Hourly</option>' +
                            '<option value="daily">Daily</option>' +
                            '<option value="weekly">Weekly</option>' +
                            '<option value="monthly">Monthly</option>' +
                            '<option value="yearly">Yearly</option>' +
                            '<option value="never">Never</option>' +
                          '</select>',
        stateSuccess: '<div class="label label-success"><i class="icon-ok"></i><span>Success</span></div>',
        stateProcessing: '<div class="progress progress-info progress-striped active"><div style="width: 100%" class="bar"><i class="icon-refresh"></i><span>Processing</span></div></div>',
        statePending: '<div class="label label-info"><i class="icon-time"></i><span>Pending</span></div>',
        stateFailure: '<div class="label label-important"><i class="icon-remove"></i><span>Failure</span></div>'

    };
};

SurferNet.SitemapPanel.Crawler = function Crawler(){};

function MD5(str){
    /*
     * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
     * Digest Algorithm, as defined in RFC 1321.
     * Copyright (C) Paul Johnston 1999 - 2000.
     * Updated by Greg Holt 2000 - 2001.
     * See http://pajhome.org.uk/site/legal.html for details.
     */

    /*
     * Convert a 32-bit number to a hex string with ls-byte first
     */
    var hex_chr = "0123456789abcdef";
    function rhex(num)
    {
      str = "";
      for(j = 0; j <= 3; j++)
        str += hex_chr.charAt((num >> (j * 8 + 4)) & 0x0F) +
               hex_chr.charAt((num >> (j * 8)) & 0x0F);
      return str;
    }

    /*
     * Convert a string to a sequence of 16-word blocks, stored as an array.
     * Append padding bits and the length, as described in the MD5 standard.
     */
    function str2blks_MD5(str)
    {
      nblk = ((str.length + 8) >> 6) + 1;
      blks = new Array(nblk * 16);
      for(i = 0; i < nblk * 16; i++) blks[i] = 0;
      for(i = 0; i < str.length; i++)
        blks[i >> 2] |= str.charCodeAt(i) << ((i % 4) * 8);
      blks[i >> 2] |= 0x80 << ((i % 4) * 8);
      blks[nblk * 16 - 2] = str.length * 8;
      return blks;
    }

    /*
     * Add integers, wrapping at 2^32. This uses 16-bit operations internally
     * to work around bugs in some JS interpreters.
     */
    function add(x, y)
    {
      var lsw = (x & 0xFFFF) + (y & 0xFFFF);
      var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
      return (msw << 16) | (lsw & 0xFFFF);
    }

    /*
     * Bitwise rotate a 32-bit number to the left
     */
    function rol(num, cnt)
    {
      return (num << cnt) | (num >>> (32 - cnt));
    }

    /*
     * These functions implement the basic operation for each round of the
     * algorithm.
     */
    function cmn(q, a, b, x, s, t)
    {
      return add(rol(add(add(a, q), add(x, t)), s), b);
    }
    function ff(a, b, c, d, x, s, t)
    {
      return cmn((b & c) | ((~b) & d), a, b, x, s, t);
    }
    function gg(a, b, c, d, x, s, t)
    {
      return cmn((b & d) | (c & (~d)), a, b, x, s, t);
    }
    function hh(a, b, c, d, x, s, t)
    {
      return cmn(b ^ c ^ d, a, b, x, s, t);
    }
    function ii(a, b, c, d, x, s, t)
    {
      return cmn(c ^ (b | (~d)), a, b, x, s, t);
    }

    /*
     * Take a string and return the hex representation of its MD5.
     */
    function calcMD5(str)
    {
      x = str2blks_MD5(str);
      a =  1732584193;
      b = -271733879;
      c = -1732584194;
      d =  271733878;

      for(i = 0; i < x.length; i += 16)
      {
        olda = a;
        oldb = b;
        oldc = c;
        oldd = d;

        a = ff(a, b, c, d, x[i+ 0], 7 , -680876936);
        d = ff(d, a, b, c, x[i+ 1], 12, -389564586);
        c = ff(c, d, a, b, x[i+ 2], 17,  606105819);
        b = ff(b, c, d, a, x[i+ 3], 22, -1044525330);
        a = ff(a, b, c, d, x[i+ 4], 7 , -176418897);
        d = ff(d, a, b, c, x[i+ 5], 12,  1200080426);
        c = ff(c, d, a, b, x[i+ 6], 17, -1473231341);
        b = ff(b, c, d, a, x[i+ 7], 22, -45705983);
        a = ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
        d = ff(d, a, b, c, x[i+ 9], 12, -1958414417);
        c = ff(c, d, a, b, x[i+10], 17, -42063);
        b = ff(b, c, d, a, x[i+11], 22, -1990404162);
        a = ff(a, b, c, d, x[i+12], 7 ,  1804603682);
        d = ff(d, a, b, c, x[i+13], 12, -40341101);
        c = ff(c, d, a, b, x[i+14], 17, -1502002290);
        b = ff(b, c, d, a, x[i+15], 22,  1236535329);

        a = gg(a, b, c, d, x[i+ 1], 5 , -165796510);
        d = gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
        c = gg(c, d, a, b, x[i+11], 14,  643717713);
        b = gg(b, c, d, a, x[i+ 0], 20, -373897302);
        a = gg(a, b, c, d, x[i+ 5], 5 , -701558691);
        d = gg(d, a, b, c, x[i+10], 9 ,  38016083);
        c = gg(c, d, a, b, x[i+15], 14, -660478335);
        b = gg(b, c, d, a, x[i+ 4], 20, -405537848);
        a = gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
        d = gg(d, a, b, c, x[i+14], 9 , -1019803690);
        c = gg(c, d, a, b, x[i+ 3], 14, -187363961);
        b = gg(b, c, d, a, x[i+ 8], 20,  1163531501);
        a = gg(a, b, c, d, x[i+13], 5 , -1444681467);
        d = gg(d, a, b, c, x[i+ 2], 9 , -51403784);
        c = gg(c, d, a, b, x[i+ 7], 14,  1735328473);
        b = gg(b, c, d, a, x[i+12], 20, -1926607734);

        a = hh(a, b, c, d, x[i+ 5], 4 , -378558);
        d = hh(d, a, b, c, x[i+ 8], 11, -2022574463);
        c = hh(c, d, a, b, x[i+11], 16,  1839030562);
        b = hh(b, c, d, a, x[i+14], 23, -35309556);
        a = hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
        d = hh(d, a, b, c, x[i+ 4], 11,  1272893353);
        c = hh(c, d, a, b, x[i+ 7], 16, -155497632);
        b = hh(b, c, d, a, x[i+10], 23, -1094730640);
        a = hh(a, b, c, d, x[i+13], 4 ,  681279174);
        d = hh(d, a, b, c, x[i+ 0], 11, -358537222);
        c = hh(c, d, a, b, x[i+ 3], 16, -722521979);
        b = hh(b, c, d, a, x[i+ 6], 23,  76029189);
        a = hh(a, b, c, d, x[i+ 9], 4 , -640364487);
        d = hh(d, a, b, c, x[i+12], 11, -421815835);
        c = hh(c, d, a, b, x[i+15], 16,  530742520);
        b = hh(b, c, d, a, x[i+ 2], 23, -995338651);

        a = ii(a, b, c, d, x[i+ 0], 6 , -198630844);
        d = ii(d, a, b, c, x[i+ 7], 10,  1126891415);
        c = ii(c, d, a, b, x[i+14], 15, -1416354905);
        b = ii(b, c, d, a, x[i+ 5], 21, -57434055);
        a = ii(a, b, c, d, x[i+12], 6 ,  1700485571);
        d = ii(d, a, b, c, x[i+ 3], 10, -1894986606);
        c = ii(c, d, a, b, x[i+10], 15, -1051523);
        b = ii(b, c, d, a, x[i+ 1], 21, -2054922799);
        a = ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
        d = ii(d, a, b, c, x[i+15], 10, -30611744);
        c = ii(c, d, a, b, x[i+ 6], 15, -1560198380);
        b = ii(b, c, d, a, x[i+13], 21,  1309151649);
        a = ii(a, b, c, d, x[i+ 4], 6 , -145523070);
        d = ii(d, a, b, c, x[i+11], 10, -1120210379);
        c = ii(c, d, a, b, x[i+ 2], 15,  718787259);
        b = ii(b, c, d, a, x[i+ 9], 21, -343485551);

        a = add(a, olda);
        b = add(b, oldb);
        c = add(c, oldc);
        d = add(d, oldd);
      }
      return rhex(a) + rhex(b) + rhex(c) + rhex(d);
    }

    return calcMD5(str);
}

(function($){

})(jQuery);