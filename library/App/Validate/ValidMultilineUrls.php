<?php
class App_Validate_ValidMultilineUrls extends Zend_Validate_Abstract{
	const INVALID = 'invalidUrl';
    const ERROR = 'error';
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::INVALID => "One or more URLs has invalid format. Please supply one valid URL per line."
    );
    /**
     * Defined by Zend_Validate_Interface
     *
     *
     * @param array $value
     * @return boolean
     */
    public function isValid ($value){
        $error = FALSE;
        $value_array = preg_split('/[\s]+/', $value);
        foreach($value_array as $url){
            if ( !empty($url) && !preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?$/', $url) )
                $error = TRUE;
        }
		if($error){
			$this->_error(self::INVALID);
			return FALSE;
		}
		else{
			return TRUE;
        }
    }
}