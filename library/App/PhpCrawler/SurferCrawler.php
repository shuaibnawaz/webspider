<?php
Zend_Loader::loadFile('PHPCrawler/PHPCrawler.class.php');

class App_PhpCrawler_SurferCrawler extends PHPCrawler {
    private $document_info = null;

    public function handleDocumentInfo(PHPCrawlerDocumentInfo $DocInfo){
        $this->document_info = $DocInfo;
    }

    public function getDocumentInfo(){
        return $this->document_info;
    }
}